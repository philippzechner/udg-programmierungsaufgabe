import { Injectable } from '@angular/core';
import { CSVTable } from '../interfaces/csvtable';

@Injectable({
  providedIn: 'root'
})
export class FilecontrolService {

  public table: CSVTable;

  constructor() { }

  /**
   * Reads file data and saves data in attribute 'table'
   * @param file 
   */
  public async readFile(file: File) {

    /* Async return value to stabilize ui perfomance */
    return new Promise((resolve, reject) => {
      /* Create a file reader for the file data */
      let fileReader: FileReader = new FileReader();
  
      /* If the file is successfully loaded continue with result data */
      fileReader.onload = async () => {
  
        /* Save data and create Object */
        let data: any = fileReader.result;

        let splittedData = data.split(";");

        let rows = new Array();
        let row = new Array();

        for(let sdd of splittedData) {

          if(sdd.includes("\n") && !sdd.includes('"')) {
            let split = sdd.split("\n");
            row.push(split[0]);
            rows.push(row.splice(0));

            row = new Array();
            row.push(split[1]);
          } else {
            row.push(sdd);
          }
        }

        let csvTable: CSVTable = {
          columnHeaders: rows[0],
          rows: rows
        }

        resolve(csvTable);

        /* Other try */
        // let dataToReturn: CSVTable = {
        //   columnHeaders: new Array(),
        //   rows: new Array()
        // }
  
        // /* Split data by \n to get all rows */
        // let splittedData = data.split("\n");
        
        // /* Get all column headers */
        // for(let columnHeader of splittedData[0].split(";")) {
        //   dataToReturn.columnHeaders.push(columnHeader);
        // }
  
        // /* Work with the other data and start by 1 to skip the column headers */
        // for(let i = 1; i < splittedData.length; i++) {
        //   let rowDatas = new Array();
  
        //   /* Add all columns to the array */
        //   let rowSplitData = splittedData[i];
        //   for(let x = 0; x < dataToReturn.columnHeaders.length; x++) {
        //     if(rowSplitData[x]) {
        //       rowDatas.push(rowSplitData[x]);
        //     } else {
        //       rowDatas.push("");
        //     }
        //   }
  
        //   dataToReturn.rows.push(rowDatas);
        // }
  
        // resolve(dataToReturn);
      }
  
      fileReader.onerror = reject;
      /* Read the file as Text */
      fileReader.readAsText(file);

    });
  }

}