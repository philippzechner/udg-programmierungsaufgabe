import { TestBed } from '@angular/core/testing';

import { FilecontrolService } from './filecontrol.service';

describe('FilecontrolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilecontrolService = TestBed.get(FilecontrolService);
    expect(service).toBeTruthy();
  });
});
