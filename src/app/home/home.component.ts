import { Component, OnInit } from '@angular/core';
import { FilecontrolService } from '../services/filecontrol.service';
import { CSVTable } from '../interfaces/csvtable';
import { Label, MultiDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  /* Local File variable */
  private file: File;
  public loading: boolean;
  public rows: Array<Array<string>>;
  public max = 0;
  public amount = 0;

  /* Chart */
  private chartLabels: Label[];
  private chartData: MultiDataSet;
  private chartType: ChartType;

  constructor(public fileControl: FilecontrolService) { }

  ngOnInit() {
    /* Store instance to access it from the keyup event */
    let instance = this;
    window.onload = () => {
      
      /* Listen for onkeyup event to update chart data */
      window.onkeyup = function() {
        if(instance.rows) {
          instance.generateDonut();
        }
      }

    }

  }


  /**
   * Opens the file selector
   */
  public openFileSelector() {
    let element: HTMLElement = document.getElementById('file-upload');
    element.click();
  }

  /**
   * Imports csv
   * @param files 
   */
  public async importCSV(files: FileList) {

    /* Read the first file selected and save it in the attribute file */
    this.file = files.item(0);

    if(this.file.name.split(".")[1] == "csv") {

      /* Reset filecontrol table */
      this.rows = null;


      this.loading = true;

      /* Generate table and store it in the attribute 'rows' */
      let csvTable: any = await this.fileControl.readFile(this.file);
      this.rows = csvTable.rows;

      /* Generate Donut-Chart */
      this.generateDonut();
    } else {
      alert("Only csv files are importable");
    }
    
  }

  /**
   * Generates csv
   */
  generateCSV() {
    
    let csv: string = "";

    /* Loop through the table data and add it with the right syntax to the result */
    for(let row of this.rows) {
      let partCSV = "";

      /* Per data add ; to seperate them */
      for(let rowData of row) {
        partCSV += rowData + ";";
      }

      /* Delete the last ; and add a linebreak */
      partCSV = partCSV.substring(0, partCSV.length - 1);
      partCSV += "\n";
      csv += partCSV;
    }

    /* Only logged because the table format sometimes has to much rows */
    console.log(csv);
    alert("CSV converted string is logged in the console");
  }

  /**
   * Generates donut
   */
  public generateDonut() {
    /* Get Chart data from the rows array */
    let chartData = this.getChartData();

    /* Update Chartdata */
    this.chartLabels = chartData.donutNames;
    this.chartData = chartData.donutData;
    this.chartType = 'doughnut';
    this.loading = false;
  }

  /**
   * Sums up all chart data
   */
  private getChartData() {
    
    let chartLabels = new Array();
    let chartAmount = new Array();

    /* Looping through all rows to collect all producttypes and count the amount of each */
    for(let row of this.rows) {
      let label = row[6];
      if(!chartLabels.includes(label)) {
        chartLabels.push(label);
      }

      /* Using the label index to easiliy refind data later on */
      let labelIndex = chartLabels.indexOf(label);
      if(!chartAmount[labelIndex]) {
        chartAmount[labelIndex] = 0;
      }
      chartAmount[labelIndex] = chartAmount[labelIndex] + 1;

    }

    return { donutNames: chartLabels, donutData: chartAmount }
  }


}
