export interface CSVTable {
    columnHeaders: Array<String>;
    rows: Array<Array<String>>
}
