/**
 * Angular Router
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

/**
 * Routes for the page.
 * Containing only one path due to it is a one pager.
 */
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

/**
 * Ng module
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
