## UDG-Programmierungsaufgabe

**Responsive Design**
1. Media Screen für 1024px, da die Font an manchen Elementen zu groß für die Monitorgröße ist-
2. Media Screen für 700px, da hier einige Elemente, wie die CSV Tabelle, falsch angezeigt wurden und einige Elemente zu groß waren.

Alle anderen Screens werden von diesen Größen beeinflusst. Zusätzlich durch die Verwendung von Flexbox musste ich wenige Media Screens verwenden.

**Fremdbibliotheken** 
1. Angular 8: Für Angular 8 habe ich mich entschieden, da es ein schnelle und effiziente Entwicklungsmöglichkeit für mich bietet. HTML Elemente unter bestimmten Bedingungen anzeigen zu lassen oder alle in einem Array vorhandenen Elemente auf der Website anzeigen zu lassen, erleichtert die Arbeit um vieles.
2. ChartJS: ChartJS ist die meiner Meinung nach beste Möglichkeit um Charts darzustellen. Zusätzlich in Angular kann man diese ganz einfach updaten.
3. MaterialDesign: Ich habe Teile (Buttons) der Material Design Elemente übernommen, da ich den schlichten Stil sehr präferiere und ansprechend finde. Besonders für ein kleines Projekt ist Material Design eine Super Designmöglichkeit.

**Zeit & Vorgehen**
1. Design: ~3 Stunden. Hierbei habe Ich mir das Grundkonzept für einen simplen One Pager im Kopf überlegt und dann in Adobe XD umgesetzt. Danach habe ich dies in HTML & CSS umgesetzt.
2. FileSystem: 7-8 Stunden. Leider habe ich keine richtige Lösung hierfür finden können, weshalb die Tabelle an manchen Stellen zu viele Werte hat. Auch der Donutchart hat manche Gruppierungen, die er nicht haben sollte. Ich habe mehrere Lösungen versucht, dann im Internet geschaut, aber keine Lösung hierfür gefunden. (Durch die vielen ; , und \n)
3. Chart: 30 Minuten. Zunächst habe ich die Library installiert und habe dann das Objekt erzeugt. Danach habe ich mit einem keylistener im window event ein Update Event programmiert.
4. Responsive Design: 20 Minuten. Hier habe ich die Seite aus allen Perspektiven angeschaut und daraus die Media Screens erschlossen. Danach habe ich die Erkenntnisse umgesetzt.

**Résumé**
Mir hat die Aufgabe viel Spaß bereitet und Ich konnte mit Freude diese bearbeiten. Leider konnte Ich keine geeignete Lösung für den Convert-Fehler finden. Ebenfalls fehlte mir die Zeit, da Ich letzte und diese Woche jeweils 3 Klausuren geschrieben hab bzw. schreibe.
Ich hoffe Ich konnte Sie trotzdem von meinen Programmier- und Designfähigkeiten überzeugen.

**Developer**
Philipp Zechner - 2019 